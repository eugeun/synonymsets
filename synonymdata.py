""" Main data files for data management and exam facility
"""
import bisect
import random
import codecs
import os
from mako.template import Template


CODEC="UTF-8"
MAKO_TEMPLATE="synonymsets.mako"

class SynonymDataException(Exception): pass
class SynonymContainerException(Exception): pass


class SynonymData(object):
    """ It represents synonym set for single titile 
    """
    
    def __init__(self, title):
        self.title = unicode(title).strip()
        self.synonyms = []
        self.synonymFromId = {}
    
    
    def add(self, synonym):
        if synonym in self.synonyms:
            raise ValueError, u"Synonym '{0}' has been already added!".format(unicode(synonym))
        self.synonyms.append(unicode(synonym).strip())
        self.synonyms.sort()
        self.synonymFromId[id(synonym)] = unicode(synonym).strip()
    
    
    def edit(self, old_synonym, new_synonym):
        self.remove(old_synonym)
        self.add(new_synonym)
    
    
    def remove(self, synonym):
        i = self.synonyms[synonym]
        del self.synonymFromId[id(self.synonyms[i])]
        del self.synonyms[i]
    
    
    def __iter__(self):
        for synonym in self.synonyms:
            yield synonym
    
    
    def pop(self):
        i = random.randrange(len(self.synonyms))
        synonym = self.synonyms.pop(i)
        del self.synonymFromId[id(synonym)]
        return synonym


class SynonymContainer(object):
    
    def __init__(self):
        self.synonymSets = []
        self.setFromId = {}
    
    
    def add(self, synonymSet):
        for pair in self.synonymSets:
            if synonymSet.title == pair[1].titlle:
                raise ValueError, u"Title '{0}' has been already added!".format(unicode(synonymSet.title))
        key = synonymSet.title
        bisect.insort_left(self.synonymSets, [key, synonymSet])
        self.setFromId[id(synonymSet)] = synonymSet
    
    
    def edit(self, oldSet, newSet):
        self.remove(oldSet)
        self.add(newSet)
    
    
    def remove(self, synonymSet):
        if not id(synonymSet) in self.setFromId:
            raise ValueError, u"Old set with title '{0}' not found!".format(synonymSet.title)
        i = bisect.bisect_left(self.synonymSets, [pair[1].title, pair[1]])
        del self.setFromId[id(synonymSet)]
        del self.synonyms[i]
    
    
    def save(self, fname):
        fh = None
        error = None
        try:
            template_filename = os.path.join(os.path.dirname(__file__), MAKO_TEMPLATE)
            mako_template = Template(filename=template_filename)
            
            #synonymSets = (pair[1] for pair in self.synonymSets)
            
            fh = codecs.open(fname, "w", CODEC)
            fh.write(mako_template.render_unicode(synonymSets=self.synonymSets))
        except (IOError, OSError), e:
            error = unicode(e)
        finally:
            if fh is not None:
                fh.close()
            if error is not None:
                raise SynonymContainerException, u"Failed to save data to '{0}': {1}".format(fname, error)


if __name__ == "__main__":
    synonymSets = SynonymContainer()
    synonymSet = SynonymData("I want very much  ")
    synonymSet.add("I fancy ")
    synonymSet.add("  I am keen on")
    synonymSet.add("I am eager to   ")
    synonymSet.add("I crave for ")
    synonymSets.add(synonymSet)
    synonymSets.save(r"/tmp/test.xml")
