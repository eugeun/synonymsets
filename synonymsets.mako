<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE SYNONYMSETS>
<synonymsets>
% for pair in synonymSets :
    <synonymset title='${pair[1].title}'>
    <synonyms>
% for synonym in pair[1]:
        <synonym>${synonym}</synonym>
% endfor
    </synonyms>
% endfor
</synonymsets>
